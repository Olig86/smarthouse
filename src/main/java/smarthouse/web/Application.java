package smarthouse.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import smarthouse.backend.SmartHouse;
import smarthouse.backend.devices.devicescontainer.Hardcoded;

@SpringBootApplication
public class Application {

    public static SmartHouse smartHouse;

    public static void main(String[] args) {
        smartHouse = new SmartHouse(new Hardcoded());
        SpringApplication.run(Application.class, args);
    }
}
