package smarthouse.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import smarthouse.backend.Rooms;

@RestController
public class Controller {

    @RequestMapping("/print")
    public String print(@RequestParam(value="text") String text) {
        Application.smartHouse.print(text);
        return "your text has been printed";
    }

    @RequestMapping("/turnOnTV")
    public String turnOnTv(@RequestParam(value="room") Rooms room) {
        Application.smartHouse.turnOnTv(room);
        return String.format("TV in room %s turned on", room);
    }
}
