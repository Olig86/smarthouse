package smarthouse.backend.devices.tv;

import smarthouse.backend.devices.tv.drivers.LGTvDriver;

public class LGTV implements TV {

    LGTvDriver driver;

    public LGTV() {
        driver = new LGTvDriver();
    }

    @Override
    public void turnOff() {
        driver.turnOff();
    }

    @Override
    public void turnOn() {
        driver.turnOn();
    }
}
