package smarthouse.backend.devices.tv;

import smarthouse.backend.devices.tv.drivers.SonyTvDriver;

public class SonyTV implements TV {

    SonyTvDriver driver;

    public SonyTV() {
        driver = new SonyTvDriver();
    }

    @Override
    public void turnOff() {
        driver.sonyOff();
    }

    @Override
    public void turnOn() {
        driver.sonyOn();
    }
}
