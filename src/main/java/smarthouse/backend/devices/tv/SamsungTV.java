package smarthouse.backend.devices.tv;

import smarthouse.backend.devices.tv.drivers.SamsungTvDriver;

public class SamsungTV implements TV {

    SamsungTvDriver driver;

    public SamsungTV() {
        driver = new SamsungTvDriver();
    }

    @Override
    public void turnOff() {
        driver.switchOff();
    }

    @Override
    public void turnOn() {
        driver.switchOn();
    }
}
