package smarthouse.backend.devices.tv.drivers;

public class SamsungTvDriver {

    public void switchOn() {
        System.out.println("Samsung tv turned on");
    }

    public void switchOff() {
        System.out.println("Samsung tv turned off");
    }
}
