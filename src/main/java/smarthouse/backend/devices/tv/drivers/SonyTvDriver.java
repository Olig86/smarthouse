package smarthouse.backend.devices.tv.drivers;

public class SonyTvDriver {

    public void sonyOn() {
        System.out.println("Sony tv turned on");
    }

    public void sonyOff() {
        System.out.println("Sony tv turned off");
    }
}
