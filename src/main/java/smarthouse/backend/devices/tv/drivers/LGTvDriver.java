package smarthouse.backend.devices.tv.drivers;

public class LGTvDriver {

    public void turnOn() {
        System.out.println("LG tv turned on");
    }

    public void turnOff() {
        System.out.println("LG tv turned off");
    }
}
