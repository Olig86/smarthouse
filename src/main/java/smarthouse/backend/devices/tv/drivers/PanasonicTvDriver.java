package smarthouse.backend.devices.tv.drivers;

public class PanasonicTvDriver {

    public void On() {
        System.out.println("Panasonic tv turned on");
    }

    public void Off() {
        System.out.println("Panasonic tv turned off");
    }
}
