package smarthouse.backend.devices.tv;

import smarthouse.backend.devices.tv.drivers.PanasonicTvDriver;

public class PanasonicTV implements TV {

    PanasonicTvDriver driver;

    public PanasonicTV() {
        driver = new PanasonicTvDriver();
    }

    @Override
    public void turnOff() {
        driver.Off();
    }

    @Override
    public void turnOn() {
        driver.On();
    }
}
