package smarthouse.backend.devices.music;

import smarthouse.backend.devices.Devices;
import smarthouse.backend.Rooms;

public abstract class MusicPlayer implements Devices {

    private Rooms room;

    public MusicPlayer(Rooms room) {
        this.room = room;
    }

    public abstract void turnOff();

    public abstract void turnOn();

    public Rooms getRoom() {
        return room;
    }
}
