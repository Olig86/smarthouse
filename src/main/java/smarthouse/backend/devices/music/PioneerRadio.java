package smarthouse.backend.devices.music;

import smarthouse.backend.Rooms;

public class PioneerRadio extends MusicPlayer{

    private Rooms room;

    public PioneerRadio(Rooms room) {
        super(room);
    }

    @Override
    public void turnOff() {
        System.out.println("music device turned off");
    }

    @Override
    public void turnOn() {
        System.out.println("music device turned on");
    }

}
