package smarthouse.backend.devices.devicescontainer;

import smarthouse.backend.Rooms;
import smarthouse.backend.devices.music.KenwoodRadio;
import smarthouse.backend.devices.music.PioneerRadio;
import smarthouse.backend.devices.printers.HPPrinter;
import smarthouse.backend.devices.tv.LGTV;
import smarthouse.backend.devices.tv.SamsungTV;
import smarthouse.backend.devices.tv.TV;
import smarthouse.backend.devices.music.MusicPlayer;
import smarthouse.backend.devices.printers.LGPrinter;
import smarthouse.backend.devices.printers.Printer;
import smarthouse.backend.devices.tv.PanasonicTV;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileLoader implements DevicesContainer{

    private static List<MusicPlayer> musicPlayerList;
    private static Printer printer;
    private static Map<Rooms, TV> roomToTv;

    public FileLoader() {

        musicPlayerList = new ArrayList<>();
        roomToTv = new HashMap<>();

        Path homeDir = Paths.get("C:","Users","Oli","IdeaProjects","SmartHouse");
        Path pathToFile = homeDir.resolve("devices.txt");

        try {
            Files.readAllLines(pathToFile).forEach(FileLoader::addDevice);
        } catch (IOException e) {
            System.out.printf("There was problem with file %s%n", pathToFile.toAbsolutePath());
        }
    }

    private static void addDevice(String line) {
        String[] device = line.split(";");
        String deviceType = device[0];
        String deviceClass = device[1];
        Rooms deviceRoom = getDeviceRoom(device[2]);

        if ("TV".equals(deviceType)) {
            if ("LGTV".equals(deviceClass)) {
                roomToTv.put(deviceRoom, new LGTV());
            }
            else if ("PanasonicTV".equals(deviceClass)) {
                roomToTv.put(deviceRoom, new PanasonicTV());
            }
            else if ("SamsungTV".equals(deviceClass)) {
                roomToTv.put(deviceRoom, new SamsungTV());
            }
        }
        else if ("Printer".equals(deviceType)) {
            if ("LGPrinter".equals(deviceClass)) {
                printer = new LGPrinter(deviceRoom);
            }
            else if ("HPPrinter".equals(deviceClass)) {
                printer = new HPPrinter(deviceRoom);
            }
        }
        else if ("MusicPlayer".equals(deviceType)) {
            if ("KenwoodRadio".equals(deviceClass)) {
                musicPlayerList.add(new KenwoodRadio(deviceRoom));
            }
            else if ("PioneerRadio".equals(deviceClass)) {
                musicPlayerList.add(new PioneerRadio(deviceRoom));
            }
        }

    }

    private static Rooms getDeviceRoom(String room) {
        return Rooms.valueOf(room);
    }

    @Override
    public List<MusicPlayer> getMusicPlayerList() {
        return musicPlayerList;
    }

    @Override
    public Printer getPrinter() {
        return printer;
    }

    @Override
    public Map<Rooms, TV> getRoomToTv() {
        return roomToTv;
    }
}

