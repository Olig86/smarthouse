package smarthouse.backend.devices.devicescontainer;

import smarthouse.backend.Rooms;
import smarthouse.backend.devices.printers.Printer;
import smarthouse.backend.devices.tv.TV;
import smarthouse.backend.devices.music.MusicPlayer;

import java.util.List;
import java.util.Map;

public interface DevicesContainer {

    List<MusicPlayer> getMusicPlayerList();
    Printer getPrinter();
    Map<Rooms, TV> getRoomToTv();

}
