package smarthouse.backend.devices.devicescontainer;

import smarthouse.backend.Rooms;
import smarthouse.backend.devices.music.KenwoodRadio;
import smarthouse.backend.devices.music.MusicPlayer;
import smarthouse.backend.devices.music.PioneerRadio;
import smarthouse.backend.devices.printers.LGPrinter;
import smarthouse.backend.devices.printers.Printer;
import smarthouse.backend.devices.tv.LGTV;
import smarthouse.backend.devices.tv.PanasonicTV;
import smarthouse.backend.devices.tv.SamsungTV;
import smarthouse.backend.devices.tv.TV;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hardcoded implements DevicesContainer{

    private List<MusicPlayer> musicPlayerList;
    private Printer printer;
    private Map<Rooms, TV> roomToTv;

    public Hardcoded() {
        addMusicPlayerList();
        addPrinter();
        addRoomToTv();
    }

    public void addMusicPlayerList() {
        musicPlayerList = new ArrayList<>();
        musicPlayerList.add(new KenwoodRadio(Rooms.LIVINGROOM));
        musicPlayerList.add(new KenwoodRadio(Rooms.BATHROOM));
        musicPlayerList.add(new PioneerRadio(Rooms.BEDROOM));
    }

    public void addPrinter() {
        printer = new LGPrinter(Rooms.LIVINGROOM);
    }

    public void addRoomToTv() {
        roomToTv = new HashMap<>();
        roomToTv.put(Rooms.BEDROOM, new LGTV());
        roomToTv.put(Rooms.LIVINGROOM, new PanasonicTV());
        roomToTv.put(Rooms.BATHROOM, new SamsungTV());
    }

    @Override
    public List<MusicPlayer> getMusicPlayerList() {
        return musicPlayerList;
    }

    @Override
    public Printer getPrinter() {
        return printer;
    }

    @Override
    public Map<Rooms, TV> getRoomToTv() {
        return roomToTv;
    }

}
