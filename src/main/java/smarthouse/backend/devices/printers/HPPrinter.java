package smarthouse.backend.devices.printers;

import smarthouse.backend.Rooms;
import smarthouse.backend.devices.printers.drivers.HPPrinterDriver;

public class HPPrinter implements Printer{

    HPPrinterDriver driver;
    private Rooms room;

    public HPPrinter(Rooms room) {
        this.room = room;
        driver = new HPPrinterDriver();
    }

    @Override
    public void print(String text) {
        driver.ptint(text);
    }

    @Override
    public void printInBlackWhite(String text) {
        System.out.println("HP printer does not print in black-white mode");
    }

    @Override
    public void turnOn() {
        driver.printerOn();
    }

    @Override
    public void turnOff() {
        driver.printerOff();
    }
}
