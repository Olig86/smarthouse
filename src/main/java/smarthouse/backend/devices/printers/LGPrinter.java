package smarthouse.backend.devices.printers;

import smarthouse.backend.Rooms;
import smarthouse.backend.devices.printers.drivers.LGPrinterDriver;

public class LGPrinter implements Printer{

    LGPrinterDriver driver;
    private Rooms room;

    public LGPrinter(Rooms room) {
        this.room = room;
        driver = new LGPrinterDriver();
    }

    @Override
    public void print(String text) {
       driver.printNow(text);
    }

    @Override
    public void printInBlackWhite(String text) {
        driver.printNowInBlackWhite(text);
    }

    @Override
    public void turnOn() {
        driver.switchOn();
    }

    @Override
    public void turnOff() {
        driver.switchOff();
    }
}
