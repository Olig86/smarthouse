package smarthouse.backend.devices.printers.drivers;

public class LGPrinterDriver {

    public void printNow(String text) {
        System.out.println("LG printer: " + text);
    }

    public void printNowInBlackWhite(String text) {
        System.out.println("LG printer in black-white mode: " + text);
    }

    public void switchOn() {
        System.out.println("LG printer switched on");
    }

    public void switchOff() {
        System.out.println("LG printer switched off");
    }

}
