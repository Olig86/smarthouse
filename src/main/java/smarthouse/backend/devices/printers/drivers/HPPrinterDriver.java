package smarthouse.backend.devices.printers.drivers;

public class HPPrinterDriver {

    public void ptint(String text) {
        System.out.println("HP printer: " + text);
    }

    public void printerOn() {
        System.out.println("HP printer is working");
    }

    public void printerOff() {
        System.out.println("HP printer shut down");
    }

}
