package smarthouse.backend.devices.printers;

import smarthouse.backend.devices.Devices;

public interface Printer extends Devices {

    void print(String text);
    void printInBlackWhite(String text);

}
