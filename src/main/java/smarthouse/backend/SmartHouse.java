package smarthouse.backend;

import smarthouse.backend.devices.Devices;
import smarthouse.backend.devices.devicescontainer.DevicesContainer;
import smarthouse.backend.devices.music.MusicPlayer;
import smarthouse.backend.devices.printers.Printer;
import smarthouse.backend.devices.tv.TV;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SmartHouse {

    private List<MusicPlayer> musicPlayerList;
    private Printer printer;
    private Map<Rooms, TV> roomToTv;
    private List<Devices> devicesList;

    public SmartHouse(DevicesContainer devicesContainer) {
        musicPlayerList = devicesContainer.getMusicPlayerList();
        printer = devicesContainer.getPrinter();
        roomToTv = devicesContainer.getRoomToTv();

        devicesList = new ArrayList<>();
        devicesList.addAll(musicPlayerList);
        devicesList.addAll(roomToTv.values());
        devicesList.add(printer);
    }


    public void turnOffAllDevices() {
        for (Devices device: devicesList) {
            device.turnOff();
        }
    }

    public void printerOn() {
        printer.turnOn();
    }

    public void printerOff() {
        printer.turnOff();
    }

    public void  print(String text){
        printerOn();
        printer.print(text);
    }

    public void printBlackWhite(String text) {
        printer.printInBlackWhite(text);
    }

    public void turnOnTv (Rooms room) {
        roomToTv.get(room).turnOn();
    }

    public void turnOffTV (Rooms room) {
        roomToTv.get(room).turnOff();
    }

    public void turnOffAllTV() {
        for (Rooms room : Rooms.values()) {
            roomToTv.get(room).turnOff();
        }

    }

    public void turnOnRadio(Rooms room) {
        for (MusicPlayer musicPlayer : musicPlayerList) {
            if(musicPlayer.getRoom() == room) {
                musicPlayer.turnOn();
            }
        }
    }

    public void turnOffRadio(Rooms room) {
        for (MusicPlayer musicPlayer : musicPlayerList) {
            if(musicPlayer.getRoom() == room) {
                musicPlayer.turnOn();
            }
        }
    }

}
