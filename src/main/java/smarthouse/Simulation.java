package smarthouse;

import smarthouse.backend.Rooms;
import smarthouse.backend.SmartHouse;
import smarthouse.backend.devices.devicescontainer.FileLoader;
import smarthouse.backend.devices.devicescontainer.Hardcoded;

public class Simulation {

    public static void main(String[] args) {

//        SmartHouse myHouse = new SmartHouse(new Hardcoded());
        SmartHouse myHouse = new SmartHouse(new FileLoader());

        myHouse.turnOnRadio(Rooms.BATHROOM);
        myHouse.print("test drukarki");
        myHouse.turnOnTv(Rooms.LIVINGROOM);
        myHouse.turnOnTv(Rooms.BATHROOM);
        myHouse.turnOnTv(Rooms.BEDROOM);

        myHouse.turnOffTV(Rooms.LIVINGROOM);
        myHouse.turnOffAllDevices();

    }

}
